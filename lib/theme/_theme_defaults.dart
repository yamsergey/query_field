import 'package:query_field/theme/_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

extension __SelfOrDefault on Object {
  T selfOrDefault<T>(T value) => (this != null) ? this as T : value;
}

class QueryFieldThemeDefaults {
  final QueryFieldTheme theme;

  const QueryFieldThemeDefaults(this.theme);

  factory QueryFieldThemeDefaults.material(ThemeData themeData) =>
      QueryFieldThemeDefaults(QueryFieldTheme.material(themeData));

  factory QueryFieldThemeDefaults.cupertino(CupertinoThemeData themeData) =>
      QueryFieldThemeDefaults(QueryFieldTheme.cupertion(themeData));

  EdgeInsets padding(EdgeInsets delegate) =>
      delegate.selfOrDefault(EdgeInsets.only(left: 10.0, right: 10.0));

  InputDecoration inputDecoration(InputDecoration delegate) =>
      delegate.selfOrDefault(InputDecoration(border: InputBorder.none));

  BoxDecoration cupertinoInputDecoration(BoxDecoration delegate) =>
      delegate.selfOrDefault(BoxDecoration(border: Border.fromBorderSide(BorderSide.none)));

  Decoration decoration(Decoration delegate) =>
      delegate.selfOrDefault(BoxDecoration(
        borderRadius: theme.borderRadius,
        border: Border.all(color: theme.borderColor),
        color: theme.backgroundColor,
        boxShadow: [
          BoxShadow(
            color: theme.shadowColor,
            offset: theme.shadowOffset, //(x,y)
            blurRadius: theme.shadowBlurRadius,
          ),
        ],
      ));
}
