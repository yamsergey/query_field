import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QueryFieldTheme {
  final Color borderColor;
  final Color backgroundColor;
  final Color shadowColor;
  final double shadowBlurRadius;
  final Offset shadowOffset;
  final BorderRadius borderRadius;

  const QueryFieldTheme(
      {@required this.borderColor,
      @required this.borderRadius,
      @required this.backgroundColor,
      @required this.shadowColor,
      @required this.shadowBlurRadius,
      @required this.shadowOffset});

  factory QueryFieldTheme.material(ThemeData material) {
    var isLightMode = material.brightness == Brightness.light;
    return QueryFieldTheme(
        borderRadius: BorderRadius.circular(20.0),
        borderColor: material.dividerColor,
        backgroundColor:
            (isLightMode) ? material.canvasColor : Color(0xff35363a),
        shadowColor: material.shadowColor.withOpacity(0.1),
        shadowBlurRadius: 2.0,
        shadowOffset: Offset(0.0, 0.5));
  }

  factory QueryFieldTheme.cupertion(CupertinoThemeData cupertino) {
    var isLightMode = cupertino.brightness == Brightness.light;
    return QueryFieldTheme(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        borderColor: Color(0x33000000),
        backgroundColor: (isLightMode)
            ? CupertinoColors.systemBackground
            : Color(0x33FFFFFF),
        shadowColor: Color(0xff35363a).withOpacity(0.1),
        shadowBlurRadius: 2.0,
        shadowOffset: Offset(0.0, 0.5));
  }
}
