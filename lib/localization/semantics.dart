import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart' show Intl;
import 'package:query_field/localization/l10n/semantics/results/messages_all.dart';

class QueryFieldSemanticsLocalizations extends WidgetsLocalizations {
  final String localeName;

  QueryFieldSemanticsLocalizations(this.localeName);

  static LocalizationsDelegate<QueryFieldSemanticsLocalizations> delegate =
      _QueryFieldSemanticsLocalizationsDelegate();

  static Future<QueryFieldSemanticsLocalizations> load(Locale locale) {
    final String name = locale.countryCode?.isEmpty ?? false
        ? locale.languageCode
        : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      return QueryFieldSemanticsLocalizations(localeName);
    });
  }

  static QueryFieldSemanticsLocalizations of(BuildContext context) {
    return Localizations.of<QueryFieldSemanticsLocalizations>(
        context, QueryFieldSemanticsLocalizations);
  }

  String get dismissLabel => Intl.message("Cancel",
      desc: "Deactivate search bar and clear entered data", locale: localeName);

  @override
  TextDirection get textDirection => TextDirection.ltr;
}

class _QueryFieldSemanticsLocalizationsDelegate
    extends LocalizationsDelegate<QueryFieldSemanticsLocalizations> {
  @override
  bool isSupported(Locale locale) => ["ru", "en"].contains(locale.languageCode);

  @override
  Future<QueryFieldSemanticsLocalizations> load(Locale locale) =>
      QueryFieldSemanticsLocalizations.load(locale);

  @override
  bool shouldReload(
          covariant LocalizationsDelegate<QueryFieldSemanticsLocalizations> old) =>
      false;
}

extension SearchBarSemanticLocalizationAccessor on BuildContext {
  QueryFieldSemanticsLocalizations get intlSearchBarSemantics =>
      QueryFieldSemanticsLocalizations.of(this);
}
