import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart' show Intl;
import 'package:query_field/localization/l10n/strings/default/results/messages_all.dart';

class QueryFieldStringsLocalizations extends WidgetsLocalizations {
  final String localeName;

  QueryFieldStringsLocalizations(this.localeName);

  static LocalizationsDelegate<QueryFieldStringsLocalizations> delegate =
      _QueryFieldMaterialLocalizationDelegate();

  static Future<QueryFieldStringsLocalizations> load(Locale locale) {
    final String name = locale.countryCode?.isEmpty ?? false
        ? locale.languageCode
        : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      return QueryFieldStringsLocalizations(localeName);
    });
  }

  static QueryFieldStringsLocalizations of(BuildContext context) {
    return Localizations.of<QueryFieldStringsLocalizations>(
        context, QueryFieldStringsLocalizations);
  }

  String get searchBartHintLabel => Intl.message("Start your search",
      name: "searchBartHintLabel",
      desc: "Hint text for the search bar, and call to start search",
      locale: localeName);

  String get searchBarDismissLabel => Intl.message("Cancel",
      name: "searchBarDismissLabel",
      desc: "Button that will dismiss search attempt",
      locale: localeName);

  @override
  TextDirection get textDirection => TextDirection.ltr;
}

class _QueryFieldMaterialLocalizationDelegate
    extends LocalizationsDelegate<QueryFieldStringsLocalizations> {
  @override
  bool isSupported(Locale locale) => ["ru", "en"].contains(locale.languageCode);

  @override
  Future<QueryFieldStringsLocalizations> load(Locale locale) =>
      QueryFieldStringsLocalizations.load(locale);

  @override
  bool shouldReload(
          covariant LocalizationsDelegate<QueryFieldStringsLocalizations>
              old) =>
      false;
}

extension SearchBarMaterialStringsLocalizationsAccessor on BuildContext {
  QueryFieldStringsLocalizations get intlSearchBarMaterialStrings {
    var instance = QueryFieldStringsLocalizations.of(this);
    assert(
        instance != null, "You have to add ${this.runtimeType} to your app.");
    return instance;
  }
}
