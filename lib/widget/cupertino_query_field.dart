import 'package:query_field/localization/strings.dart';
import 'package:query_field/localization/semantics.dart';
import 'package:query_field/theme/_theme_defaults.dart';
import 'package:query_field/query_field.dart';
import 'package:query_field/widget/eared.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoQueryField extends QueryField {
  final List<Widget> leading;
  final List<Widget> trailing;
  final Decoration decoration;
  final BoxDecoration inputDecoration;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final CupertinoThemeData theme;
  final FocusNode focusNode;
  final VoidCallback onDismissed;
  final ValueChanged<String> onChanged;
  final TextEditingController textController;
  final String hintText;
  final TextStyle style;
  final TextStyle hintStyle;
  final Duration duration;
  final String dismissText;
  final QueryFieldSemanticsLocalizations semanticsLocalization;
  final QueryFieldStringsLocalizations strings;
  final Icon clearIcon;

  static const _defaultDuration = Duration(milliseconds: 200);

  const CupertinoQueryField(
      {Key key,
      this.leading,
      this.trailing,
      this.decoration,
      this.inputDecoration,
      this.padding,
      this.margin,
      this.theme,
      this.focusNode,
      this.onDismissed,
      this.onChanged,
      this.textController,
      this.hintText,
      this.style,
      this.hintStyle,
      this.dismissText,
      this.duration = _defaultDuration,
      this.semanticsLocalization,
      this.strings,
      this.clearIcon})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => CupertinoQueryFieldState();
}

class CupertinoQueryFieldState extends State<CupertinoQueryField>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  FocusNode focusNode;
  TextEditingController textController;
  QueryFieldSemanticsLocalizations semantics;
  QueryFieldStringsLocalizations strings;

  bool _isInEditMode = false;
  final _barKey = GlobalKey();
  final _cancelKey = GlobalKey();
  Size initialBarSize;

  static const double _defaultCupertinoTextFieldHeight = 34.0;

  @override
  void initState() {
    super.initState();
    focusNode = widget.focusNode ?? FocusNode();
    textController = widget.textController ?? TextEditingController();
    focusNode.addListener(onFocusChanged);
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback(_layoutCallback);
  }

  @override
  void didChangeMetrics() {
    initialBarSize = null;
  }

  void _layoutCallback(Duration _) {
    initialBarSize = initialBarSize ?? _barKey.size();
    debugPrint("on layout bar width: ${_barKey.size().width}");
  }

  void onFocusChanged() {
    setState(() {
      _isInEditMode = focusNode.hasFocus;
    });
  }

  bool get isInEditMode => _isInEditMode;

  set isInEditMode(bool enabled) {
    setState(() {
      this._isInEditMode = enabled;
    });
  }

  @override
  Widget build(BuildContext context) {
    var defaults = QueryFieldThemeDefaults.cupertino(
        widget.theme ?? CupertinoTheme.of(context));
    semantics = widget.semanticsLocalization ?? context.intlSearchBarSemantics;
    strings = widget.strings ?? context.intlSearchBarMaterialStrings;

    return _initialView(context, defaults, _constrainedBar(context, defaults));
  }

  Widget _initialView(
      BuildContext context, QueryFieldThemeDefaults defaults, Widget bar) {
    return Stack(children: [_cancelRow(context, defaults), bar]);
  }

  Widget _constrainedBar(
      BuildContext context, QueryFieldThemeDefaults defaults) {
    var screenWidth = MediaQuery.of(context).size.width;
    var barWidth = initialBarSize?.width ?? screenWidth;
    var cancelWidth = _cancelKey.size()?.width ?? 0;
    var width = (isInEditMode || textController.text.isNotEmpty)
        ? barWidth - cancelWidth - 20
        : barWidth;

    return AnimatedContainer(
        duration: widget.duration ?? CupertinoQueryField._defaultDuration,
        constraints: BoxConstraints(minWidth: 0, maxWidth: width),
        width: width,
        child: Eared(
          key: _barKey,
          margin: widget.margin,
          decoration: defaults.decoration(widget.decoration),
          padding: defaults.padding(widget.padding),
          leading: (widget.leading != null && widget.leading.isNotEmpty)
              ? Row(children: widget.leading)
              : SizedBox.shrink(),
          trailing: _trailing(widget.trailing),
          middle: _queryTextField(context),
        ));
  }

  Widget _cancelRow(BuildContext context, QueryFieldThemeDefaults defaults) {
    return Container(
        height: _defaultCupertinoTextFieldHeight,
        margin: widget.margin,
        padding: defaults.padding(widget.padding),
        child: Row(children: [
          Expanded(child: SizedBox.expand()),
          _cancel(key: _cancelKey)
        ]));
  }

  Widget _trailing(List<Widget> requestedTrailing) {
    if (isInEditMode || textController.text.isNotEmpty) {
      return GestureDetector(
          onTap: () {
            textController.clear();
          },
          child: widget.clearIcon ??
              Icon(CupertinoIcons.clear_circled_solid,
                  size: 20.0, color: CupertinoColors.inactiveGray));
    } else {
      return (requestedTrailing != null && requestedTrailing.isNotEmpty)
          ? Row(children: requestedTrailing)
          : SizedBox.shrink();
    }
  }

  Widget _cancel({Key key}) => Semantics(
      label: semantics.dismissLabel,
      child: CupertinoButton(
          padding: EdgeInsets.zero,
          onPressed: () {
            textController.clear();
            if (focusNode.hasFocus) {
              focusNode.unfocus();
            } else {
              isInEditMode = false;
            }
            if (widget.onDismissed != null) widget.onDismissed();
          },
          child: Center(
              child: AnimatedDefaultTextStyle(
                  duration: Duration(
                      milliseconds: (widget.duration ??
                              CupertinoQueryField._defaultDuration)
                          .inMilliseconds),
                  curve: Curves.easeInOutBack,
                  style: (isInEditMode || textController.text.isNotEmpty)
                      ? _theme(context).textTheme.actionTextStyle
                      : _theme(context)
                          .textTheme
                          .actionTextStyle
                          .copyWith(color: Colors.transparent),
                  child: Text(
                      widget.dismissText ?? strings.searchBarDismissLabel,
                      key: key)))));

  Widget _queryTextField(BuildContext context) {
    var theme = widget.theme ?? CupertinoTheme.of(context);
    var defaults = QueryFieldThemeDefaults.cupertino(theme);

    return CupertinoTextField(
      focusNode: focusNode,
      decoration: defaults.cupertinoInputDecoration(widget.inputDecoration),
      placeholder: widget.hintText ?? strings.searchBartHintLabel,
      placeholderStyle: widget.hintStyle,
      style: widget.style ??
          theme.textTheme.textStyle
              .copyWith(color: CupertinoColors.inactiveGray),
      onChanged: widget.onChanged,
      controller: textController,
    );
  }

  CupertinoThemeData _theme(BuildContext context) =>
      widget.theme ?? CupertinoTheme.of(context);
}

extension __SizeFromGlobalKey on GlobalKey {
  Size size() {
    RenderBox renderBox = this.currentContext?.findRenderObject() as RenderBox;
    return renderBox?.size;
  }
}
