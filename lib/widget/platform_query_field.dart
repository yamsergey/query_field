import 'package:query_field/localization/semantics.dart';
import 'package:query_field/localization/strings.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:query_field/query_field.dart';
import 'platform/_cupertino_query_field_data.dart';
import 'platform/_material_query_field_data.dart';

class PlatformQueryField
    extends PlatformWidgetBase<CupertinoQueryField, MaterialQueryField> {
  final PlatformBuilder<MaterialQueryFieldData> material;
  final PlatformBuilder<CupertinoQueryFieldData> cupertino;

  final List<Widget> leading;
  final List<Widget> trailing;
  final Decoration decoration;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final FocusNode focusNode;
  final VoidCallback onDismissed;
  final ValueChanged<String> onChanged;
  final TextEditingController textController;
  final String hintText;
  final TextStyle style;
  final TextStyle hintStyle;
  final QueryFieldSemanticsLocalizations semanticsLocalization;
  final QueryFieldStringsLocalizations strings;
  final Icon clearIcon;

  PlatformQueryField(
      {Key key,
      this.leading,
      this.trailing,
      this.decoration,
      this.padding,
      this.margin,
      this.focusNode,
      this.onDismissed,
      this.onChanged,
      this.textController,
      this.hintText,
      this.style,
      this.hintStyle,
      this.semanticsLocalization,
      this.strings,
      this.clearIcon,
      this.material,
      this.cupertino});

  @override
  CupertinoQueryField createCupertinoWidget(BuildContext context) {
    var data =
        (cupertino != null) ? cupertino(context, platform(context)) : null;

    return CupertinoQueryField(
        key: key,
        leading: data?.leading ?? leading,
        trailing: data?.trailing ?? trailing,
        decoration: data?.decoration ?? decoration,
        inputDecoration: data?.inputDecoration,
        padding: data?.padding ?? padding,
        margin: data?.margin ?? margin,
        theme: data?.theme,
        focusNode: data?.focusNode ?? focusNode,
        onDismissed: data?.onDismissed ?? onDismissed,
        onChanged: data?.onChanged ?? onChanged,
        textController: data?.textController ?? textController,
        hintText: data?.hintText ?? hintText,
        style: data?.style ?? style,
        hintStyle: data?.hintStyle ?? hintStyle,
        dismissText: data?.dismissText,
        duration: data?.duration,
        semanticsLocalization:
            data?.semanticLocalization ?? semanticsLocalization,
        strings: data?.strings ?? strings,
        clearIcon: data?.clearIcon ?? clearIcon);
  }

  @override
  MaterialQueryField createMaterialWidget(BuildContext context) {
    var data = (material != null) ? material(context, platform(context)) : null;

    return MaterialQueryField(
        key: key,
        leading: data?.leading ?? leading,
        trailing: data?.trailing ?? trailing,
        decoration: data?.decoration ?? decoration,
        inputDecoration: data?.inputDecoration,
        padding: data?.padding ?? padding,
        margin: data?.margin ?? margin,
        theme: data?.theme,
        focusNode: data?.focusNode ?? focusNode,
        onDismissed: data?.onDismissed ?? onDismissed,
        onChanged: data?.onChanged ?? onChanged,
        textController: data?.textController ?? textController,
        hintText: data?.hintText ?? hintText,
        style: data?.style ?? style,
        hintStyle: data?.hintStyle ?? hintStyle,
        semanticLocalization:
            data?.semanticsLocalization ?? semanticsLocalization,
        strings: data?.strings ?? strings,
        clearIcon: data?.clearIcon ?? clearIcon);
  }
}
