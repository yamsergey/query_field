import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Eared extends Container {
  final Widget leading;
  final Widget middle;
  final Widget trailing;
  final Decoration decoration;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final Color color;
  final double height;
  final double width;

  Eared(
      {Key key,
      this.leading,
      this.trailing,
      this.decoration,
      this.padding,
      this.margin,
      this.middle,
      this.color,
      this.height,
      this.width})
      : super(
            key: key,
            color: color,
            margin: margin,
            height: height,
            width: width,
            padding: padding,
            decoration: decoration,
            child: Row(children: [leading, Expanded(child: middle), trailing]));
}
