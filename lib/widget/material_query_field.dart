import 'package:query_field/localization/strings.dart';
import 'package:query_field/theme/_theme_defaults.dart';
import 'package:query_field/query_field.dart';
import 'package:query_field/widget/eared.dart';
import 'package:flutter/material.dart';
import 'package:query_field/localization/semantics.dart';

class MaterialQueryField extends QueryField {
  final ThemeData theme;
  final FocusNode focusNode;
  final TextEditingController textController;
  final VoidCallback onDismissed;
  final ValueChanged<String> onChanged;
  final String hintText;
  final TextStyle style;
  final TextStyle hintStyle;
  final InputDecoration inputDecoration;
  final QueryFieldSemanticsLocalizations semanticLocalization;
  final QueryFieldStringsLocalizations strings;
  final Icon dismissIcon;
  final Icon clearIcon;

  const MaterialQueryField(
      {Key key,
      this.theme,
      List<Widget> leading,
      List<Widget> trailing,
      Decoration decoration,
      EdgeInsets padding,
      EdgeInsets margin,
      this.focusNode,
      this.textController,
      this.onDismissed,
      this.onChanged,
      this.hintText,
      this.style,
      this.hintStyle,
      this.inputDecoration,
      this.semanticLocalization,
      this.strings,
      this.dismissIcon,
      this.clearIcon})
      : super(
            key: key,
            leading: leading,
            trailing: trailing,
            decoration: decoration,
            padding: padding,
            margin: margin);

  @override
  State<StatefulWidget> createState() => MaterialQueryFieldState();
}

class MaterialQueryFieldState extends State<MaterialQueryField> {
  FocusNode focusNode;
  TextEditingController textController;
  QueryFieldSemanticsLocalizations semantics;
  QueryFieldStringsLocalizations strings;
  QueryFieldThemeDefaults themeDefaults;

  bool _isInEditMode = false;

  bool get isInEditMode => _isInEditMode;

  ThemeData theme(BuildContext context) => widget.theme ?? Theme.of(context);

  set isInEditMode(bool enabled) {
    setState(() {
      this._isInEditMode = enabled;
    });
  }

  @override
  void initState() {
    super.initState();
    focusNode = widget.focusNode ?? FocusNode();
    textController = widget.textController ?? TextEditingController();
    focusNode.addListener(onFocusChanged);
  }

  void onFocusChanged() {
    setState(() {
      _isInEditMode = focusNode.hasFocus;
    });
  }

  @override
  Widget build(BuildContext context) {
    themeDefaults = QueryFieldThemeDefaults.material(theme(context));
    semantics = widget.semanticLocalization ?? context.intlSearchBarSemantics;
    strings = widget.strings ?? context.intlSearchBarMaterialStrings;

    return Eared(
      margin: widget.margin,
      decoration: themeDefaults.decoration(widget.decoration),
      padding: themeDefaults.padding(widget.padding),
      leading: _leading(context),
      trailing: _trailing(),
      middle: _queryField(context),
    );
  }

  Widget _leading(BuildContext context) {
    if (isInEditMode || textController.text.isNotEmpty) {
      return Row(children: [
        Semantics(
            label: semantics.dismissLabel,
            child: IconButton(
                color: theme(context).iconTheme.color,
                autofocus: false,
                onPressed: () {
                  textController.clear();
                  if (widget.onDismissed != null) widget.onDismissed();
                  if (focusNode.hasFocus) {
                    focusNode.unfocus();
                  } else {
                    isInEditMode = false;
                  }
                },
                icon: widget.dismissIcon ?? Icon(Icons.arrow_back)))
      ]);
    } else {
      return (widget.leading != null && widget.leading.isNotEmpty)
          ? Row(children: widget.leading)
          : SizedBox.shrink();
    }
  }

  Widget _trailing() {
    if (isInEditMode || textController.text.isNotEmpty) {
      return Builder(builder: (context) {
        return IconButton(
            color: theme(context).iconTheme.color,
            onPressed: () {
              textController.clear();
            },
            icon: widget.clearIcon ?? Icon(Icons.clear));
      });
    } else {
      return (widget.trailing != null && widget.trailing.isNotEmpty)
          ? Row(children: widget.trailing)
          : SizedBox.shrink();
    }
  }

  Widget _queryField(BuildContext context) {
    var defaults =
        QueryFieldThemeDefaults.material(theme(context) ?? Theme.of(context));
    var fallbackTextStyle = theme(context)
        .textTheme
        .bodyText1
        .copyWith(color: theme(context).textTheme.caption.color);

    var query = TextField(
      focusNode: focusNode,
      decoration: defaults.inputDecoration(widget.inputDecoration).copyWith(
          hintText: widget.hintText ?? strings.searchBartHintLabel,
          hintStyle: widget.hintStyle ?? fallbackTextStyle),
      style: widget.style ?? fallbackTextStyle,
      onChanged: widget.onChanged,
      controller: textController,
    );
    return query;
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }
}
