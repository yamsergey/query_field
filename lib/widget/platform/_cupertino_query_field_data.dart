import 'package:flutter/cupertino.dart';
import 'package:query_field/localization/semantics.dart';
import 'package:query_field/localization/strings.dart';
import 'package:flutter/widgets.dart';

class CupertinoQueryFieldData {
  final List<Widget> leading;
  final List<Widget> trailing;
  final Decoration decoration;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final CupertinoThemeData theme;
  final FocusNode focusNode;
  final TextEditingController textController;
  final VoidCallback onDismissed;
  final ValueChanged<String> onChanged;
  final String hintText;
  final TextStyle style;
  final TextStyle hintStyle;
  final BoxDecoration inputDecoration;
  final QueryFieldSemanticsLocalizations semanticLocalization;
  final QueryFieldStringsLocalizations strings;
  final Icon dismissIcon;
  final Icon clearIcon;
  final Duration duration;
  final String dismissText;

  CupertinoQueryFieldData(
      {this.leading,
        this.trailing,
        this.decoration,
        this.padding,
        this.margin,
        this.theme,
        this.focusNode,
        this.textController,
        this.onDismissed,
        this.onChanged,
        this.hintText,
        this.style,
        this.hintStyle,
        this.inputDecoration,
        this.semanticLocalization,
        this.strings,
        this.dismissIcon,
        this.clearIcon,
        this.duration,
        this.dismissText});
}
