
import 'package:flutter/material.dart';
import 'package:query_field/localization/strings.dart';
import 'package:query_field/localization/semantics.dart';
import 'package:flutter/widgets.dart';

class MaterialQueryFieldData {
  final List<Widget> leading;
  final List<Widget> trailing;
  final Decoration decoration;
  final InputDecoration inputDecoration;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final ThemeData theme;
  final FocusNode focusNode;
  final VoidCallback onDismissed;
  final ValueChanged<String> onChanged;
  final TextEditingController textController;
  final String hintText;
  final TextStyle style;
  final TextStyle hintStyle;
  final String dismissText;
  final QueryFieldSemanticsLocalizations semanticsLocalization;
  final QueryFieldStringsLocalizations strings;
  final Icon clearIcon;

  MaterialQueryFieldData(
      {this.leading,
        this.trailing,
        this.decoration,
        this.inputDecoration,
        this.padding,
        this.margin,
        this.theme,
        this.focusNode,
        this.onDismissed,
        this.onChanged,
        this.textController,
        this.hintText,
        this.style,
        this.hintStyle,
        this.dismissText,
        this.semanticsLocalization,
        this.strings,
        this.clearIcon});
}
