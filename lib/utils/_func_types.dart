import 'package:flutter/widgets.dart';

typedef VoidCallback = void Function();
typedef VoidInputCallBack<I> = void Function(I input);
typedef FunctionCallback<R> = R Function();
typedef FunctionInputCallback<R, I> = R Function(I input);

typedef VoidContextCallback = void Function(BuildContext context);
typedef VoidContextInputCallBack<I> = void Function(
    BuildContext context, I input);
typedef FunctionContextCallback<R> = R Function(BuildContext context);
typedef FunctionContextInputCallback<R, I> = R Function(
    BuildContext context, I input);