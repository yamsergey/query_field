library query_field;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

export 'package:query_field/widget/material_query_field.dart';
export 'package:query_field/widget/cupertino_query_field.dart';

abstract class QueryField extends StatefulWidget {
  final List<Widget> leading;
  final List<Widget> trailing;
  final Decoration decoration;
  final EdgeInsets padding;
  final EdgeInsets margin;

  const QueryField(
      {Key key,
      this.leading,
      this.trailing,
      this.decoration,
      this.padding,
      this.margin})
      : super(key: key);
}
