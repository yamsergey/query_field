import 'dart:ui';
import 'package:query_field/localization/semantics.dart';
import 'package:query_field/localization/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:query_field/widget/material_query_field.dart';
import 'package:query_field/widget/cupertino_query_field.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          QueryFieldStringsLocalizations.delegate,
          QueryFieldSemanticsLocalizations.delegate,
        ],
        locale: Locale('en'),
        supportedLocales: [Locale('en'), Locale('ru')],
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: Column(children: [
          material(ThemeData.from(colorScheme: ColorScheme.dark())),
          material(ThemeData.from(colorScheme: ColorScheme.light())),
          cupertino(context, Brightness.light),
          cupertino(context, Brightness.dark)
        ])));
  }

  // @override
  // Widget build(BuildContext context) {
  //   return PlatformProvider(
  //       initialPlatform: TargetPlatform.android,
  //       builder: (BuildContext context) =>
  //           Builder(builder: (BuildContext context) {
  //             return PlatformApp(
  //                 localizationsDelegates: [
  //                   GlobalMaterialLocalizations.delegate,
  //                   GlobalWidgetsLocalizations.delegate,
  //                   GlobalCupertinoLocalizations.delegate,
  //                   SearchFieldStringsLocalizations.delegate,
  //                   SearchFieldSemanticsLocalizations.delegate,
  //                 ],
  //                 locale: Locale('en'),
  //                 supportedLocales: [Locale('en'), Locale('ru')],
  //                 debugShowCheckedModeBanner: false,
  //                 home: PlatformScaffold(body: PlatformSearchField()));
  //           }));
  // }

  Widget material(ThemeData theme) {
    return MaterialQueryField(
        margin: EdgeInsets.only(top: 60.0, left: 20.0, right: 20.0),
        theme: theme,
        leading: [
          IconButton(
            icon: Icon(Icons.search, color: theme.iconTheme.color),
          )
        ],
        trailing: [
          Icon(Icons.verified_user_outlined, color: theme.iconTheme.color),
          Icon(Icons.data_usage_rounded, color: theme.iconTheme.color)
        ]);
  }

  Widget cupertino(
          BuildContext context, Brightness brightness) =>
      CupertinoQueryField(
          margin: EdgeInsets.only(top: 60.0, left: 20.0, right: 20.0),
          theme: CupertinoTheme.of(context).copyWith(brightness: brightness),
          leading: [
            Icon(CupertinoIcons.search, color: CupertinoColors.inactiveGray)
          ],
          trailing: [
            Icon(CupertinoIcons.checkmark_shield,
                color: CupertinoColors.inactiveGray)
          ]);
}
