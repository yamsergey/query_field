
intl_semantics_gen_arb:
	flutter pub run intl_translation:extract_to_arb --output-dir=lib/localization/l10n/semantics/arb lib/localization/semantics.dart

intl_semantics_gen_results:
	flutter pub run intl_translation:generate_from_arb --output-dir=lib/localization/l10n/semantics/results --no-use-deferred-loading lib/localization/semantics.dart lib/localization/l10n/semantics/arb/intl_*.arb

intl_defaults_gen_arb:
	flutter pub run intl_translation:extract_to_arb --output-dir=lib/localization/l10n/strings/default/arb lib/localization/strings.dart

intl_defaults_gen_results:
	flutter pub run intl_translation:generate_from_arb --output-dir=lib/localization/l10n/strings/default/results --no-use-deferred-loading lib/localization/strings.dart lib/localization/l10n/strings/default/arb/intl_*.arb

	
