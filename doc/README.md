# query_field

A Flutter's widget that represent search fields for different platforms.

Currently there are two designs supported:

* Material `MaterialSearchField` 

  > Inspired by search field from Google's material apps

* Cupertion `CupertinoSearchField`

  > Inspired by search field from Apple's apps

Preview (You can find source code for the preview under `example` directory):

![Preview](query_field_animation.gif)

## Getting Started

Usage of the field quite similart to usage of `TextField`. The widget has `leading` and `trailing` widgets which will dissapear when user start typing and text control buttons (`dissmiss` and `clear`) will appear. 



**Material search field minimal example:**

```
MaterialSearchField(
    onChanged: (text) {},
    onDismissed: () {},
)
```

**Cupertino search field minimal example:**

```
CupertinoSearchField(
    onChanged: (text) {},
    onDismissed: () {},
)
```

**Use as Platform Widget:**

The library supports [Flutter Platform Widgets](https://pub.dev/packages/flutter_platform_widgets)

```
PlatformSearchField(
    onChanged: (text) {},
    onDismissed: () {},
)
```



## Internationalization 

You can provide your own strings for: `hintText` and  `dismissText` directly by:

* Widget constructor
* Providing `SearchFieldStringsLocalizations` as localisation delegate
* Contributing to the repository (The package support *flutter_intl*, or try to do that)