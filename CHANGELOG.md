## [0.1.4] - 26 December 2020

* Remove intl_translation dependency, because it start confliction with flutter_internationalization
*
## [0.1.3] - 26 December 2020

* Make dependencies wider

## [0.1.2] - 26 December 2020

* Fix dependencies

## [0.1.1] - 26 December 2020

* First release
