import 'package:query_field/query_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils/utils.dart';

void main() {
  testWidgets('Should show correct hint text', (tester) async {
    await tester.pumpWidget(material(MaterialQueryField(hintText: "test hint")));
    await tester.pump();

    final hintFinder = find.text("test hint");

    expect(hintFinder, findsOneWidget);
  });

  testWidgets("Should call onChange when changed", (tester) async {
    var key = UniqueKey();
    var enteredText = "";
    await tester.pumpWidget(material(MaterialQueryField(
        key: key, onChanged: (value) => enteredText = value)));
    await tester.pump();

    final barFinder = find.byKey(key);
    await tester.tap(barFinder);
    await tester.showKeyboard(barFinder);
    await tester.enterText(barFinder, "Test text entering");
    expect(enteredText, "Test text entering");
  });

  testWidgets("Should call onDismissed when dismissed", (tester) async {
    var semantics = await tester.appSemantics();

    var dismissed = false;

    var key = UniqueKey();
    await tester.pumpWidget(material(
        MaterialQueryField(key: key, onDismissed: () => dismissed = true)));
    await tester.pump();
    await tester.tap(find.byKey(key));
    await tester.pump();
    await tester.tap(find.bySemanticsLabel(semantics.dismissLabel));
    expect(dismissed, true);
  });
}
