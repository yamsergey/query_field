import 'package:query_field/query_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils/utils.dart';

void main() {
  testWidgets('Should show correct hint text', (tester) async {
    await tester
        .pumpWidget(material(CupertinoQueryField(hintText: "test hint")));
    await tester.pump();

    final hintFinder = find.text("test hint");

    expect(hintFinder, findsOneWidget);
  });

  testWidgets("Should call onChange when changed", (tester) async {
    var key = UniqueKey();
    var enteredText = "";
    await tester.pumpWidget(material(CupertinoQueryField(
        key: key, onChanged: (value) => enteredText = value)));
    await tester.pump();

    final barFinder = find.byKey(key);
    await tester.tap(barFinder);
    await tester.showKeyboard(barFinder);
    await tester.enterText(barFinder, "Test text entering");
    expect(enteredText, "Test text entering");
  });

  testWidgets("Should call onDismissed when dismissed", (tester) async {
    var semantics = await tester.appSemantics();

    var dismissed = false;

    var key = UniqueKey();
    await tester.pumpWidget(material(Container(width: 400, child: CupertinoQueryField(
        key: key, onDismissed: () => dismissed = true, duration: Duration()))));
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(key));
    await tester.pumpAndSettle();
    // TODO: Somehow semantics label appears to be duplicated and joined by '\n'. Have to be investigated.
    await tester.tap(find.bySemanticsLabel("${semantics.dismissLabel}\n${semantics.dismissLabel}"));
    expect(dismissed, true);
  });
}
