import 'dart:ui';

import 'package:query_field/localization/semantics.dart';
import 'package:flutter_test/flutter_test.dart';

extension SemanticLocalizationAccessor on WidgetTester {
  Future<QueryFieldSemanticsLocalizations> appSemantics({Locale locale}) async {
    return QueryFieldSemanticsLocalizations.load(locale ?? Locale("en"));
  }
}
