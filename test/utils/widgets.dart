import 'package:query_field/localization/strings.dart';
import 'package:query_field/localization/semantics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget material(Widget child, {ThemeData theme}) =>
    MaterialApp(localizationsDelegates: [
      QueryFieldSemanticsLocalizations.delegate,
      QueryFieldStringsLocalizations.delegate
    ], supportedLocales: [
      Locale('en')
    ], locale: Locale('en'), theme: theme, home: Scaffold(body: child));

Widget cupertino(Widget child, {CupertinoThemeData theme}) => CupertinoApp(
    localizationsDelegates: [
      QueryFieldSemanticsLocalizations.delegate,
      QueryFieldStringsLocalizations.delegate]
    , supportedLocales: [Locale('en')],
    locale: Locale('en'),
    theme: theme,
    home: CupertinoPageScaffold(child: child));
